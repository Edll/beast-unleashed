﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown("escape")) {
			print("Escape space key was pressed");
			//SceneManager.LoadScene("Main menu");
		}
	}

	public void BackToMainMenu()
	{
		SceneManager.LoadScene("Main menu");
	}
}